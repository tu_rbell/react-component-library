import React, {Component} from 'react'
import Header from '../../components/Header/Header'

class Login extends Component {

  render () {
    return (
      <div className="path--login">
        <Header transparent={false} />

        <main className="spacer">
          <h1>Login</h1>
        </main>
      </div>
    )
  }
}

export default Login