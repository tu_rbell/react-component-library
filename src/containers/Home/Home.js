import React, {Component} from 'react'
import ud from '../../content/User/Data'
import Header from '../../components/Header/Header'
import Banner from '../../components/Banner/Banner'
import CreditRank from '../../components/CreditRank/CreditRank'
import Grades from '../../content/Credit/Grades'
import History from '../../content/Credit/History'

class Home extends Component {

  constructor(props) {
    super(props)
    this.state = {
      score: 0,
      grades: Grades,
      history: History
    }

    this.handleScoreChange = this.handleScoreChange.bind(this);
    this.calculateGrade = this.calculateGrade.bind(this);
  }

  componentDidMount() {
    this.setState({
      score: ud.scores.vantage3.score
    });
  }

  handleScoreChange(e) {
    this.setState({
      score: e.target.value
    });
  }

  calculateGrade() {
    let score = this.state.score;
    let letterGrade = 'F';
    this.state.grades.map(function (range) {
      if (score >= range.min && score <= range.max) {
        letterGrade = range.copy;
        return letterGrade;
      }
      return letterGrade;
    });
    return letterGrade;
  }

  render () {
    return (
      <div className="path--home">
        
        <Header transparent={true} />

        <main>

          <Banner
            title='TransUnion'
            body='Information for Good'
            stamp={true}
          />

          <label>Score Preview</label><br />
          <input type="number" name="score" value={this.state.score} onChange={(e) => {this.handleScoreChange(e)}}/>

          <CreditRank 
            layout='column'
            calculateGrade={this.calculateGrade}
            score={this.state.score}
          />

        </main>
      </div>
    )
  }
}

export default Home