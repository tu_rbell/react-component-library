import React from 'react'
import { Link, NavLink } from 'react-router-dom'
import styles from './Footer.module.scss'

const Footer = props => {

  return (
    <footer className={styles.footer}>
      <div className={styles.container}>

        <ul className={styles.menu}>
          <li>
            <span className={styles.title} tabIndex="0">Account</span>
            <ul>
              <li><NavLink to="/login" activeClassName={styles.active} className={styles.link}>Log In</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Sign Up</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Dashboard</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Profile</NavLink></li>
              <li className="hidden"><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Sign Out</NavLink></li>
            </ul>
          </li>

          <li>
            <span className={styles.title} tabIndex="0">Credit</span>
            <ul>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Credit Alerts</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Credit Lock</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Credit Score</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Credit Report</NavLink></li>
            </ul>
          </li>

          <li>
            <span className={styles.title} tabIndex="0">Resources</span>
            <ul>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>About</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Blog</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Education</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>FAQs</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Savings</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Tools</NavLink></li>
            </ul>
          </li>

          <li>
            <span className={styles.title} tabIndex="0">Support</span>
            <ul>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Contact</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Dispute Center</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Privacy Policy</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Terms of Use</NavLink></li>
              <li><NavLink to="NULL" activeClassName={styles.active} className={styles.link}>Sitemap</NavLink></li>
            </ul>
          </li>
        </ul>

        <ul className={styles.social}>
          <li>
            <span className={styles.title} tabIndex="0">Social Media</span>
            <ul>
              <li><Link to="NULL" className={styles.link}>Facebook</Link></li>
              <li><Link to="NULL" className={styles.link}>Twitter</Link></li>
              <li><Link to="NULL" className={styles.link}>YouTube</Link></li>
              <li><Link to="NULL" className={styles.link}>Linkedin</Link></li>
            </ul>
          </li>
        </ul>

        <div className={styles.copyright}>
          <small>This site is hosted and operated by TransUnion Consumer Interactive, Inc., a wholly owned subsidiary of TransUnion, LLC. © Copyright {(new Date().getFullYear())} TransUnion Consumer Interactive. All Rights Reserved.</small>
        </div>
        
      </div>
    </footer>
  )
}

export default Footer