import React, { Component } from 'react'
import { Link, NavLink } from 'react-router-dom'
import { ReactComponent as Logo } from './logo.svg'
import './Header.scss'

class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isTop: true
    };
    this.onScroll = this.onScroll.bind(this);
  }

  componentDidMount() {
    document.addEventListener('scroll', () => {
      const isTop = window.scrollY < 100;
      if(isTop !== this.state.isTop) {
        this.onScroll(isTop);
      }
    });
  }

  onScroll(isTop) {
    this.setState({isTop});
  }

  render() {
    return (
      <header className={`header ${this.props.transparent ? 'header--transparent' : ''} ${this.state.isTop ? '' : 'header--scrolled'}`}>
        <div className="container">

          <Link to='/'>
            <Logo className="header__logo" />
          </Link>

          <ul className="header__menu header__menu--alpha">
            <li tabIndex="1">
              <span>Account</span>
              <ul className="header__menu--beta">
                <li><NavLink to="/login">Log In</NavLink></li>
                <li><NavLink to="NULL">Sign Up</NavLink></li>
                <li><NavLink to="NULL">Dashboard</NavLink></li>
                <li><NavLink to="NULL">Profile</NavLink></li>
                <li className="hidden"><NavLink to="NULL">Sign Out</NavLink></li>
              </ul>
            </li>

            <li tabIndex="1">
              <span>Credit</span>
              <ul className="header__menu--beta">
                <li><NavLink to="NULL">Credit Alerts</NavLink></li>
                <li><NavLink to="NULL">Credit Lock</NavLink></li>
                <li><NavLink to="NULL">Credit Score</NavLink></li>
                <li><NavLink to="NULL">Credit Report</NavLink></li>
              </ul>
            </li>

            <li tabIndex="1">
              <span>Resources</span>
              <ul className="header__menu--beta">
                <li><NavLink to="NULL">About</NavLink></li>
                <li><NavLink to="NULL">Blog</NavLink></li>
                <li><NavLink to="NULL">Education</NavLink></li>
                <li><NavLink to="NULL">FAQs</NavLink></li>
                <li><NavLink to="NULL">Savings</NavLink></li>
                <li><NavLink to="NULL">Tools</NavLink></li>
              </ul>
            </li>

            <li tabIndex="1">
              <span>Support</span>
              <ul className="header__menu--beta">
                <li><NavLink to="NULL">Contact</NavLink></li>
                <li><NavLink to="NULL">Dispute Center</NavLink></li>
                <li><NavLink to="NULL">Privacy Policy</NavLink></li>
                <li><NavLink to="NULL">Terms of Use</NavLink></li>
                <li><NavLink to="NULL">Sitemap</NavLink></li>
              </ul>
            </li>
          </ul>

          <Link to="NULL" className="header__button">Sign Up</Link>

        </div>
      </header>
    )
  }
}

export default Header