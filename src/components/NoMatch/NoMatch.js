import React from 'react'
import Header from '../Header/Header'
import Banner from '../Banner/Banner'

const NoMatch = props => {

  return (
    <div class="path--404">
      <Header 
        transparent={true}
      />

      <Banner
        title='404'
        body='Sorry, this page does not exist'
        stamp={true}
      />
    </div>
  )
}

export default NoMatch