import React from 'react'
import { ReactComponent as Stamp } from './stamp.svg'
import styles from './Banner.module.scss'

const Banner = props => {
  return (

    <div className={`${styles.banner} ${props.stamp ? styles.background : ''}`}>
      <div className={styles.container}>

        {props.title &&
          <h1 className={styles.title}>{props.title}</h1>
        }

        {props.body &&
          <p className={styles.body}>{props.body}</p>
        }

      </div>

      {props.stamp &&
        <Stamp className={styles.stamp} />
      }
    </div>

  )
}

export default Banner