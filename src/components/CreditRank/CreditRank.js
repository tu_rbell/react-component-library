import React, { Component } from 'react'
import Header from './Header/Header'
import './CreditRank.scss'

class CreditRank extends Component {

  constructor(props) {
    super(props)
    this.calculateGrade = this.props.calculateGrade.bind(this)
  }

  render() {
    let letterGrade = this.calculateGrade()
    let placeholderScoreComparison = (this.props.score * .1).toFixed(0) // TODO: get the correctly calculated value
    let rowScorePosition = (((this.props.score - 300) / (850 - 300) * 100) * .975) + '%'

    if (this.props.layout === 'row') {
      this.gradeStyle = {
        left: rowScorePosition
      }
    }

    return (
      <section className={`rank rank--${this.props.layout}`}>

        <Header 
          title = 'Where You Stand'
          body = {`Your TransUnion Credit Score is currently higher than ${placeholderScoreComparison}% of American consumers.`}
        />

        <ul className="rank__chart">
          <li className="rank__group rank__group--A grade__color--A">
            <span className="rank__percentage">74%-100%</span>
            <span className="rank__status">Excellent</span>
            <span className="rank__range">
              <span className="rank__range--min">781</span>
              <span className="rank__range--seperator">-</span>
              <span className="rank__range--max">850</span>
              <span className="rank__grade"> / A</span>
            </span>
          </li>
          <li className="rank__group rank__group--B grade__color--B">
            <span className="rank__percentage">52%-73%</span>
            <span className="rank__status">Very Good</span>
            <span className="rank__range">
              <span className="rank__range--min">720</span>
              <span className="rank__range--seperator">-</span>
              <span className="rank__range--max">780</span>
              <span className="rank__grade"> / B</span>
            </span>
          </li>
          <li className="rank__group rank__group--C grade__color--C">
            <span className="rank__percentage">35%-51%</span>
            <span className="rank__status">Good</span>
            <span className="rank__range">
              <span className="rank__range--min">658</span>
              <span className="rank__range--seperator">-</span>
              <span className="rank__range--max">719</span>
              <span className="rank__grade"> / C</span>
            </span>
          </li>
          <li className="rank__group rank__group--D grade__color--D">
            <span className="rank__percentage">22%-34%</span>
            <span className="rank__status">Fair</span>
            <span className="rank__range">
              <span className="rank__range--min">601</span>
              <span className="rank__range--seperator">-</span>
              <span className="rank__range--max">657</span>
              <span className="rank__grade"> / D</span>
            </span>
          </li>
          <li className="rank__group rank__group--F grade__color--F">
            <span className="rank__percentage">1%-21%</span>
            <span className="rank__status">Poor</span>
            <span className="rank__range">
              <span className="rank__range--min">300</span>
              <span className="rank__range--seperator">-</span>
              <span className="rank__range--max">600</span>
              <span className="rank__grade"> / F</span>
            </span>
          </li>

          <span className={`rank__indicator rank__indicator--${letterGrade} grade__color--${letterGrade}`} style={this.gradeStyle}>
            {this.props.score}
          </span>
        </ul>
      </section>
    )
  }
}

export default CreditRank;