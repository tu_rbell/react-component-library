import React from 'react'

const Header = props => {
  return (

    <div className="rank__header">

        {props.title &&
          <h2 className="rank__title">{props.title}</h2>
        }

        {props.body &&
          <p className="rank__body">{props.body}</p>
        }

    </div>
    
  )
}

export default Header