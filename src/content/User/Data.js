const Data = {
  ResponseDetails: {
    ServerResponse: 'Success',
    Server: 0,
    visitToken: 'a5bc2404-3cb8-46c8-b0bb-913d609b2086',
    csrfToken: 'coUYLq2J8rt47LLLsAL2szH7BPQov30yE8W8UXRqzqt4eNTcC2WaGDJpueYjsHAsmKHDcT30gGa270l7nYTZWVBRer50D+620NzMvfwx6rk='
  },
  scores: {
    vantage3: {
      source: 'SingleCreditReport',
      date: '02/28/2019',
      refreshdate: '03/28/2019',
      refreshdatetime: '2019-03-28T10:52:52.000-07:00',
      score: 810,
      factors: {
        positive: [
          {
            explanation: 'explain: You have no bankruptcy on your credit report, which increases your score.',
            factor: 'factor: There is no bankruptcy on your credit report',
            whatyoucando: 'cando: Keep paying bills on time every month since it is important for maintaining a good credit score. If you remain behind with any payments, bring them current as soon as possible, and then make future payments on time. Over time, this will have a positive impact on your score.'
          },
          {
            explanation: 'explain: Public records include information filed or recorded by local, state, federal or other government agencies that is available to the general public. The types of public records that can affect your score include legal judgments against you and tax liens levied by a government authority. You have few or no public records on your credit report, which increases your score.',
            factor: 'factor: You have few or no unsatisfied public records on your credit report',
            whatyoucando: 'cando: Pay all bills on-time and satisfy all judgments. The impact that negative items such as public records have on your credit score will diminish over time.'
          }
        ],
        negative: [
          {
            explanation: 'explain: The VantageScore credit score model relies on information in your credit files at the three national credit reporting companies (Equifax, Experian and TransUnion) to generate your score. Your credit file does not have enough credit behavior information about your loans. A mix of different types of open and active credit accounts can have a positive impact on your credit score.',
            factor: 'factor: Lack of sufficient relevant account information',
            whatyoucando: 'cando: Maintaining open and active credit accounts in good standing can help improve your credit score.'
          },
          {
            explanation: 'explain: Some collection agencies report account information to credit bureaus just like lenders do. Your credit file has too many accounts that have been sent to a collection agency and remain unpaid. Unpaid collection accounts in your file can have a significant negative impact on your credit score.',
            factor: 'factor: You have too many collection agency accounts that are unpaid',
            whatyoucando: 'cando: Satisfy all collection accounts and pay all other accounts on time each month. The impact that negative items such as collections accounts have on your credit score will diminish over time.'
          },
          {
            explanation: 'explain: Bankcard accounts include credit cards and charge cards from a bank and are frequently revolving accounts. Revolving accounts allow you to carry a balance and your monthly payment will vary, based on the amount of your balance. The VantageScore credit score model relies on information in your credit files at the three national credit reporting companies (Equifax, Experian and TransUnion) to generate your score. Your credit file does not have enough credit behavior information about your bankcard or revolving accounts. A mix of different types of open and active credit accounts, including bankcard and other revolving accounts, can have a positive impact on your credit score.',
            factor: 'factor: Lack of sufficient relevant bankcard or revolving account information',
            whatyoucando: 'cando: Maintaining open and active credit accounts in good standing can help improve your credit score.'
          }
        ]
      }
    },
    transrisk: {}
  },
  report: {
    date: '02/28/2019',
    balances: 1989,
    inquiries: 4,
    latepmnts: [
      {
        late30: 4,
        late60: 0,
        late90: 0
      },
      {
        late30: 3,
        late60: 0,
        late90: 0
      },
      {
        late30: 0,
        late60: 0,
        late90: 0
      },
      {
        late30: 0,
        late60: 0,
        late90: 0
      },
      {
        late30: 1,
        late60: 0,
        late90: 0
      }
    ],
    openaccts: [
      {
        opendate: '08/01/1997'
      },
      {
        opendate: '08/01/1997'
      },
      {
        opendate: '10/01/1998'
      },
      {
        opendate: '08/01/1997'
      }
    ],
    revolving: [
      {
        opendate: '08/01/1997',
        openstatus: 'O',
        currbal: 276,
        limit: 276
      },
      {
        opendate: '08/01/1997',
        openstatus: 'O',
        currbal: 296,
        limit: 400
      },
      {
        opendate: '10/01/1998',
        openstatus: 'O',
        currbal: 1201,
        limit: 1201
      },
      {
        opendate: '08/01/1997',
        openstatus: 'O',
        currbal: 216,
        limit: 216
      }
    ],
    credit: []
  },
  alerts: {
    alerts: {
      isMonitored: {},
      tuc: [],
      eqf: [],
      exp: []
    }
  },
  trending: [
    {
      date: '01/28/2019',
      score: 565
    },
    {
      date: '02/28/2019',
      score: 810
    }
  ],
  simulator: {},
  reportstu: {
    isFrozen: {
      TUC: false,
      EXP: false,
      EQF: false
    },
    ReportDate: {
      TUC: '07/28/1999'
    },
    CreditScore: {
      TUC: 810
    },
    Name: {
      TUC: 'Anne Link Test'
    },
    AKA: {
      TUC: 0,
      EXP: 0,
      EQF: 0
    },
    DOB: {
      TUC: '09/01/1957'
    },
    CurrentAddr: {
      TUC: '1071 SW 101ST HOLLYWOOD FL 33025',
      EXP: 0,
      EQF: 0
    },
    CurrentAddrDate: {
      TUC: '08/01/1997',
      EXP: 0,
      EQF: 0
    },
    PreviousAddr: {
      TUC: [],
      EXP: [],
      EQF: []
    },
    Employer: {
      TUC: 0,
      EXP: 0,
      EQF: 0
    },
    EmployerDate: {
      TUC: 0,
      EXP: 0,
      EQF: 0
    },
    PreviousEmployer: {
      TUC: [],
      EXP: [],
      EQF: []
    },
    'OLD-PreviousEmployer': {
      TUC: 0,
      EQF: 0,
      EXP: 0
    },
    ConsumerStatement: {
      TUC: [],
      EQF: [],
      EXP: []
    },
    OpenAccts: {
      TUC: 4,
      EXP: 0,
      EQF: 0,
      MERGE: 0
    },
    TotalAccounts: {
      TUC: 5,
      EXP: 0,
      EQF: 0,
      MERGE: 0
    },
    TotalBalances: {
      TUC: 1989,
      EXP: 0,
      EQF: 0,
      MERGE: 0
    },
    CloseAccounts: {
      TUC: 1,
      EXP: 0,
      EQF: 0,
      MERGE: 0
    },
    TotalMonthlyPayments: {
      TUC: 64,
      EXP: 0,
      EQF: 0,
      MERGE: 0
    },
    DelinquentAccounts: {
      TUC: 2,
      EXP: 0,
      EQF: 0,
      MERGE: 0
    },
    DerogatoryAccounts: {
      TUC: 0,
      EXP: 0,
      EQF: 0,
      MERGE: 0
    },
    PublicRecords: {
      TUC: 0,
      EXP: 0,
      EQF: 0,
      MERGE: 0
    },
    InquirySummary: {
      TUC: 4,
      EXP: 0,
      EQF: 0,
      MERGE: 0
    },
    PubRecs: {
      Bankruptcy: [],
      LegalItem: [],
      Garnishment: [],
      MaritalItem: [],
      MiscPublicRecord: [],
      FinancialCounseling: [],
      FinancingStatement: [],
      Foreclosure: [],
      TaxLien: []
    },
    Inquiries: [
      {
        Bureau: 'TUC',
        Owner: 'TRUE LINK',
        DateOfInquiry: '07/28/1999'
      },
      {
        Bureau: 'TUC',
        Owner: 'CREDCO',
        DateOfInquiry: '03/24/1999'
      },
      {
        Bureau: 'TUC',
        Owner: 'CREDCO',
        DateOfInquiry: '03/18/1999'
      },
      {
        Bureau: 'TUC',
        Owner: 'SEARS',
        DateOfInquiry: '07/20/1998'
      }
    ],
    Accounts: {
      RealEstate: [
        {
          accountName: {
            TUC: 'MERC BK OF ST LOUIS NA'
          },
          originalCreditor: {
            TUC: 0
          },
          accountNumber: {
            TUC: '36461**'
          },
          currentBalance: {
            TUC: 0
          },
          dateAccountStatus: {
            TUC: '04/01/1999'
          },
          dateOpened: {
            TUC: '07/01/1995'
          },
          dateReported: {
            TUC: '04/01/1999'
          },
          highBalance: {
            TUC: 119200
          },
          dateClosed: {
            TUC: '04/01/1999'
          },
          monthlyPayment: {
            TUC: 1286
          },
          accountCondition: {
            TUC: 'Closed'
          },
          termMonths: {
            TUC: 360
          },
          responsibility: {
            TUC: 'Maker'
          },
          remarks: {
            TUC: 'Closed'
          },
          accountType: {
            TUC: 'Conventional real estate mortgage'
          },
          late30Count: {
            TUC: 3
          },
          late60Count: {
            TUC: 0
          },
          late90Count: {
            TUC: 0
          },
          PayStatus: {
            TUC: 1
          },
          WorstPayStatus: {
            TUC: 1
          },
          CreditorName: {
            TUC: 'MERC B SL NA'
          },
          CreditorPhone: {
            TUC: 8009556372
          },
          CreditorAddress: {
            TUC: 'MTG SERV TRAM 32-1 POB 524'
          },
          CreditorCity: {
            TUC: 'ST LOUIS'
          },
          CreditorState: {
            TUC: 'MO'
          },
          CreditorZip: {
            TUC: 63166
          },
          dateLastPayment: {
            TUC: '03/01/1999'
          },
          amountPastDue: {
            TUC: 0
          },
          CreditLimit: {
            TUC: 0
          },
          oldestYear: {
            Year: 1999
          },
          'PaymentHistory-TUC': {
            M28: 'U',
            MY28: '4-1999',
            M27: 1,
            MY27: '3-1999',
            M26: 'C',
            MY26: '2-1999',
            M25: 1,
            MY25: '1-1999',
            M24: 'C',
            MY24: '12-1998',
            M23: 'C',
            MY23: '11-1998',
            M22: 'C',
            MY22: '10-1998',
            M21: 'C',
            MY21: '9-1998',
            M20: 'C',
            MY20: '8-1998',
            M19: 'C',
            MY19: '7-1998',
            M18: 'C',
            MY18: '6-1998',
            M17: 'C',
            MY17: '5-1998',
            M16: 1,
            MY16: '4-1998',
            M15: 'C',
            MY15: '3-1998',
            M14: 'C',
            MY14: '2-1998',
            M13: 'C',
            MY13: '1-1998',
            M12: 'C',
            MY12: '12-1997',
            M11: 'C',
            MY11: '11-1997',
            M10: 0,
            MY10: '10-1997',
            M9: 0,
            MY9: '9-1997',
            M8: 0,
            MY8: '8-1997',
            M7: 0,
            MY7: '7-1997',
            M6: 0,
            MY6: '6-1997',
            M5: 0,
            MY5: '5-1997'
          }
        }
      ],
      Revolving: [
        {
          accountName: {
            TUC: 'HRSI BANK-WHIRL'
          },
          originalCreditor: {
            TUC: 0
          },
          accountNumber: {
            TUC: '1590577****'
          },
          currentBalance: {
            TUC: 276
          },
          dateAccountStatus: {
            TUC: '07/01/1999'
          },
          dateOpened: {
            TUC: '08/01/1997'
          },
          dateReported: {
            TUC: '07/01/1999'
          },
          highBalance: {
            TUC: 364
          },
          dateClosed: {
            TUC: 0
          },
          monthlyPayment: {
            TUC: 20
          },
          accountCondition: {
            TUC: 'Open'
          },
          termMonths: {
            TUC: 0
          },
          responsibility: {
            TUC: 'Individual'
          },
          remarks: {
            TUC: 0
          },
          accountType: {
            TUC: 'Charge account'
          },
          late30Count: {
            TUC: 4
          },
          late60Count: {
            TUC: 0
          },
          late90Count: {
            TUC: 0
          },
          PayStatus: {
            TUC: 1
          },
          WorstPayStatus: {
            TUC: 1
          },
          CreditorName: {
            TUC: 'HHLD BANK'
          },
          CreditorPhone: {
            TUC: 0
          },
          CreditorAddress: {
            TUC: 'P.O. BOX 746'
          },
          CreditorCity: {
            TUC: 'WOOD DALE'
          },
          CreditorState: {
            TUC: 'IL'
          },
          CreditorZip: {
            TUC: 60191
          },
          dateLastPayment: {
            TUC: '05/01/1999'
          },
          amountPastDue: {
            TUC: 20
          },
          CreditLimit: {
            TUC: 276
          },
          oldestYear: {
            Year: 1999
          },
          'PaymentHistory-TUC': {
            M31: 1,
            MY31: '7-1999',
            M30: 'C',
            MY30: '6-1999',
            M29: 1,
            MY29: '5-1999',
            M28: 'C',
            MY28: '4-1999',
            M27: 'C',
            MY27: '3-1999',
            M26: 1,
            MY26: '2-1999',
            M25: 'C',
            MY25: '1-1999',
            M24: 'C',
            MY24: '12-1998',
            M23: 'C',
            MY23: '11-1998',
            M22: 'C',
            MY22: '10-1998',
            M21: 'C',
            MY21: '9-1998',
            M20: 'C',
            MY20: '8-1998',
            M19: 'C',
            MY19: '7-1998',
            M18: 'C',
            MY18: '6-1998',
            M17: 'C',
            MY17: '5-1998',
            M16: 'C',
            MY16: '4-1998',
            M15: 'C',
            MY15: '3-1998',
            M14: 'C',
            MY14: '2-1998',
            M13: 1,
            MY13: '1-1998',
            M12: 'C',
            MY12: '12-1997',
            M11: 'C',
            MY11: '11-1997',
            M10: 0,
            MY10: '10-1997',
            M9: 0,
            MY9: '9-1997',
            M8: 0,
            MY8: '8-1997'
          }
        },
        {
          accountName: {
            TUC: 'BURDINES'
          },
          originalCreditor: {
            TUC: 0
          },
          accountNumber: {
            TUC: '225563****'
          },
          currentBalance: {
            TUC: 296
          },
          dateAccountStatus: {
            TUC: '06/01/1999'
          },
          dateOpened: {
            TUC: '08/01/1997'
          },
          dateReported: {
            TUC: '06/01/1999'
          },
          highBalance: {
            TUC: 513
          },
          dateClosed: {
            TUC: 0
          },
          monthlyPayment: {
            TUC: 7
          },
          accountCondition: {
            TUC: 'Open'
          },
          termMonths: {
            TUC: 0
          },
          responsibility: {
            TUC: 'Individual'
          },
          remarks: {
            TUC: 0
          },
          accountType: {
            TUC: 'Charge account'
          },
          late30Count: {
            TUC: 0
          },
          late60Count: {
            TUC: 0
          },
          late90Count: {
            TUC: 0
          },
          PayStatus: {
            TUC: 'C'
          },
          WorstPayStatus: {
            TUC: 'C'
          },
          CreditorName: {
            TUC: 'BURDIN/FDSNB'
          },
          CreditorPhone: {
            TUC: 8004772167
          },
          CreditorAddress: {
            TUC: '13141 34TH ST N'
          },
          CreditorCity: {
            TUC: 'CLEARWATER'
          },
          CreditorState: {
            TUC: 'FL'
          },
          CreditorZip: {
            TUC: 34622
          },
          dateLastPayment: {
            TUC: '05/01/1999'
          },
          amountPastDue: {
            TUC: 0
          },
          CreditLimit: {
            TUC: 400
          },
          oldestYear: {
            Year: 1999
          },
          'PaymentHistory-TUC': {
            M30: 'C',
            MY30: '6-1999',
            M29: 'C',
            MY29: '5-1999',
            M28: 'C',
            MY28: '4-1999',
            M27: 'C',
            MY27: '3-1999',
            M26: 'C',
            MY26: '2-1999',
            M25: 'C',
            MY25: '1-1999',
            M24: 'C',
            MY24: '12-1998',
            M23: 'C',
            MY23: '11-1998',
            M22: 'C',
            MY22: '10-1998',
            M21: 'C',
            MY21: '9-1998',
            M20: 'C',
            MY20: '8-1998',
            M19: 'C',
            MY19: '7-1998',
            M18: 'C',
            MY18: '6-1998',
            M17: 'C',
            MY17: '5-1998',
            M16: 'C',
            MY16: '4-1998',
            M15: 'C',
            MY15: '3-1998',
            M14: 'C',
            MY14: '2-1998',
            M13: 'C',
            MY13: '1-1998',
            M12: 'C',
            MY12: '12-1997',
            M11: 'C',
            MY11: '11-1997',
            M10: 0,
            MY10: '10-1997',
            M9: 0,
            MY9: '9-1997',
            M8: 0,
            MY8: '8-1997',
            M7: 0,
            MY7: '7-1997'
          }
        },
        {
          accountName: {
            TUC: 'CITIBANK NA'
          },
          originalCreditor: {
            TUC: 0
          },
          accountNumber: {
            TUC: '54**'
          },
          currentBalance: {
            TUC: 1201
          },
          dateAccountStatus: {
            TUC: '06/01/1999'
          },
          dateOpened: {
            TUC: '10/01/1998'
          },
          dateReported: {
            TUC: '06/01/1999'
          },
          highBalance: {
            TUC: 0
          },
          dateClosed: {
            TUC: 0
          },
          monthlyPayment: {
            TUC: 17
          },
          accountCondition: {
            TUC: 'Open'
          },
          termMonths: {
            TUC: 0
          },
          responsibility: {
            TUC: 'Individual'
          },
          remarks: {
            TUC: 0
          },
          accountType: {
            TUC: 'Credit Card'
          },
          late30Count: {
            TUC: 0
          },
          late60Count: {
            TUC: 0
          },
          late90Count: {
            TUC: 0
          },
          PayStatus: {
            TUC: 'C'
          },
          WorstPayStatus: {
            TUC: 'C'
          },
          CreditorName: {
            TUC: 'CITIBANK MC'
          },
          CreditorPhone: {
            TUC: 8008430777
          },
          CreditorAddress: {
            TUC: 'POB 6241'
          },
          CreditorCity: {
            TUC: 'SIOUX FALLS'
          },
          CreditorState: {
            TUC: 'SD'
          },
          CreditorZip: {
            TUC: 57117
          },
          dateLastPayment: {
            TUC: '05/01/1999'
          },
          amountPastDue: {
            TUC: 0
          },
          CreditLimit: {
            TUC: 1201
          },
          oldestYear: {
            Year: 1999
          },
          'PaymentHistory-TUC': {
            M30: 'C',
            MY30: '6-1999',
            M29: 'C',
            MY29: '5-1999',
            M28: 'C',
            MY28: '4-1999',
            M27: 'C',
            MY27: '3-1999',
            M26: 'C',
            MY26: '2-1999',
            M25: 'C',
            MY25: '1-1999',
            M24: 'C',
            MY24: '12-1998',
            M23: 'C',
            MY23: '11-1998',
            M22: 'C',
            MY22: '10-1998',
            M21: 0,
            MY21: '9-1998',
            M20: 0,
            MY20: '8-1998',
            M19: 0,
            MY19: '7-1998',
            M18: 0,
            MY18: '6-1998',
            M17: 0,
            MY17: '5-1998',
            M16: 0,
            MY16: '4-1998',
            M15: 0,
            MY15: '3-1998',
            M14: 0,
            MY14: '2-1998',
            M13: 0,
            MY13: '1-1998',
            M12: 0,
            MY12: '12-1997',
            M11: 0,
            MY11: '11-1997',
            M10: 0,
            MY10: '10-1997',
            M9: 0,
            MY9: '9-1997',
            M8: 0,
            MY8: '8-1997',
            M7: 0,
            MY7: '7-1997'
          }
        },
        {
          accountName: {
            TUC: 'HRSI BANK'
          },
          originalCreditor: {
            TUC: 0
          },
          accountNumber: {
            TUC: '101590577****'
          },
          currentBalance: {
            TUC: 216
          },
          dateAccountStatus: {
            TUC: '11/01/1998'
          },
          dateOpened: {
            TUC: '08/01/1997'
          },
          dateReported: {
            TUC: '11/01/1998'
          },
          highBalance: {
            TUC: 364
          },
          dateClosed: {
            TUC: 0
          },
          monthlyPayment: {
            TUC: 20
          },
          accountCondition: {
            TUC: 'Open'
          },
          termMonths: {
            TUC: 0
          },
          responsibility: {
            TUC: 'Individual'
          },
          remarks: {
            TUC: 0
          },
          accountType: {
            TUC: 0
          },
          late30Count: {
            TUC: 1
          },
          late60Count: {
            TUC: 0
          },
          late90Count: {
            TUC: 0
          },
          PayStatus: {
            TUC: 'C'
          },
          WorstPayStatus: {
            TUC: 1
          },
          CreditorName: {
            TUC: 'HHLD BANK'
          },
          CreditorPhone: {
            TUC: 0
          },
          CreditorAddress: {
            TUC: 'P.O. BOX 746'
          },
          CreditorCity: {
            TUC: 'WOOD DALE'
          },
          CreditorState: {
            TUC: 'IL'
          },
          CreditorZip: {
            TUC: 60191
          },
          dateLastPayment: {
            TUC: '04/01/1998'
          },
          amountPastDue: {
            TUC: 0
          },
          CreditLimit: {
            TUC: 216
          },
          oldestYear: {
            Year: 1998
          },
          'PaymentHistory-TUC': {
            M35: 'C',
            MY35: '11-1998',
            M34: 'C',
            MY34: '10-1998',
            M33: 'C',
            MY33: '9-1998',
            M32: 'C',
            MY32: '8-1998',
            M31: 'U',
            MY31: '7-1998',
            M30: 'C',
            MY30: '6-1998',
            M29: 'C',
            MY29: '5-1998',
            M28: 'C',
            MY28: '4-1998',
            M27: 'C',
            MY27: '3-1998',
            M26: 'C',
            MY26: '2-1998',
            M25: 1,
            MY25: '1-1998',
            M24: 'C',
            MY24: '12-1997',
            M23: 'U',
            MY23: '11-1997',
            M22: 'C',
            MY22: '10-1997',
            M21: 'C',
            MY21: '9-1997',
            M20: 0,
            MY20: '8-1997',
            M19: 0,
            MY19: '7-1997',
            M18: 0,
            MY18: '6-1997',
            M17: 0,
            MY17: '5-1997',
            M16: 0,
            MY16: '4-1997',
            M15: 0,
            MY15: '3-1997',
            M14: 0,
            MY14: '2-1997',
            M13: 0,
            MY13: '1-1997',
            M12: 0,
            MY12: '12-1996'
          }
        }
      ],
      Installment: [],
      CreditCard: [],
      Collection: [],
      Other: [],
      Unknown: []
    }
  }
}

export default Data;