const Grades = [
  {
    min: 781,
    max: 850,
    rank: '37%',
    perc: 37,
    copy: 'A'
  },
  {
    min: 720,
    max: 780,
    rank: '12%',
    perc: 12,
    copy: 'B'
  },
  {
    min: 658,
    max: 719,
    rank: '18%',
    perc: 18,
    copy: 'C'
  },
  {
    min: 601,
    max: 657,
    rank: '21%',
    perc: 21,
    copy: 'D'
  },
  {
    min: 300,
    max: 600,
    rank: '12%',
    perc: 12,
    copy: 'F'
  }
]

export default Grades