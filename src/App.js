import React, {Component} from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import './styles/index.scss'
import ScrollToTop from './components/ScrollToTop/ScrollToTop'
import Footer from './components/Footer/Footer'
import Home from './containers/Home/Home'
import Login from './containers/Login/Login'
import NoMatch from './components/NoMatch/NoMatch'

class App extends Component {

  render() {
    return (       
      <div className="App">

        <BrowserRouter>
          <ScrollToTop>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/login' component={Login} />
              <Route component={NoMatch} />
            </Switch>
          
            <Footer />
          </ScrollToTop>
        </BrowserRouter>
        
      </div>
    );
  }
}

export default App;